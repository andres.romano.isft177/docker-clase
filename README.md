# Stack completo
## Materias:

+ Bases de Datos
+ Algoritmos y Estructuras 2

Esta configuración de docker es la que utilizaremos para
los ejemplos y trabajos realizados en clase, para que 
todos tengamos la misma configuracion y evitar errores.

> Esta compuesto por MySQL, PhpMyAdmin, MongoDB, PHP 7.4 y Apache2

### Contenedores

+ database: Contiene la version 5.7 del motor MySQL
+ mongo: Contiene la ultima version de MongoDB
+ admin: Administrador web de Bases de Datos, PhpMyAdmin
+ public: Dentro podemos alojar nuestros proyectos, contiene PHP y Apache

### Variables de entorno

Se debe crear el archivo .env en la raiz del proyecto "docker-clase/.env", con las siguientes variables
``` 
TZ=America/Argentina/Buenos_Aires
SQL_SERVER=database 
MYSQL_ROOT_PASSWORD=root 
PMA_HOST=mysqldb
MONGO_INITDB_ROOT_USERNAME=root
MONGO_INITDB_ROOT_PASSWORD=root
```
### Levantar los contenedores

Comando para iniciar los contenedores, ingresando a la carpeta docker-clase/
```
sudo docker-compose up -d
```

### Accesos Web

+ http://localhost:8051 -> PhpMyAdmin
+ http://localhost:8050/mi_proyecto -> servidor apache

> mi_proyecto seria el nombre de la carpeta de nuestro proyecto.

### Accesos bash

Para poder acceder a las terminales de los contenedores
```
sudo docker exec -it nombre_contenedor bash
```
> nombre_contenedor se debe reemplazar por el nombre correspondiente al contenedor que necesitamos ingresar.

### Nota

Para poder ver las carpetas y sea más fácil el acceso desde el navegador, dentro de la carpeta public existe el archivo .htaccess con una única línea de código
```
Options +Indexes
```

Prof. Andrés D. Romano
